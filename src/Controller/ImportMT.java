package Controller;

import Model.DistributorModel;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Iterator;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import View.FrameBaru;
import Model.MasterKonversi;
import Model.SalesModel;
import View.FrameStokMT;
import example.RikzaTesting;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import utility.KoneksiDB;

public class ImportMT extends SwingWorker<Void, Void> {

    private String lokasi, datanya, SQL_Insert;
    private String[] DataToInsert;
    private int i, numRow, input;
    public JOptionPane mohonTungguDialog = new JOptionPane();
    FrameStokMT frBaru;
    MasterKonversi mk;
    MasterKonversiService mks;
    SalesModel sm;
    private String idDepoMasterKonverter;
    private Connection con;
    private Statement s;
    DistributorModel mDistributor;
    static Stack<String> stackAllDepo = new Stack<String>();
    static Stack<String> stackAllIDDepo = new Stack<String>();
    Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
//    private String catatDepoPerbaruan[];
    private Stack<String> stackDepoPerbaruan = new Stack<String>();

    public ImportMT(String fileExcel) throws SQLException {
        con = KoneksiDB.getKoneksi();
        frBaru = new FrameStokMT();
        this.lokasi = fileExcel;
    }

    @Override
    protected Void doInBackground() throws Exception {
        mohonTunggu();
        String satuanProduk = "";
        int indexBaris = 0;
        try {
//            con.setAutoCommit(false);
            s = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            datanya = "";
            /* proses membaca file excel */
            System.out.println(lokasi);
            InputStream ExcelFileToRead = new FileInputStream(lokasi);
            XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
            XSSFSheet sheet = wb.getSheetAt(0);
            numRow = sheet.getPhysicalNumberOfRows();
            XSSFRow row;
            XSSFCell cell;
            Iterator rows = sheet.rowIterator();

            mk = new MasterKonversi();
            mks = new MasterKonversiService(con);
            sm = new SalesModel(con);

            mDistributor = new DistributorModel(con);
//            mDistributor.selectTable(mk.getDistributorSelected());
//            getAllDepo(mDistributor.getIdDistributor());
            while (rows.hasNext()) {
                input++;
                row = (XSSFRow) rows.next();
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_BLANK:
                            datanya += "0~";
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                String hasil = df.format(cell.getDateCellValue());
                                datanya += hasil + "~";
                            } else {
                                DecimalFormat df = new DecimalFormat("###.###");
                                String hasil = df.format(cell.getNumericCellValue());
                                datanya += hasil + "~";
                            }
                            break;
                        case Cell.CELL_TYPE_STRING:
                            datanya += cell.getStringCellValue().toString() + "~";
                            break;
                    }
                }
                /* Proses Insert ke database */
                DataToInsert = datanya.split("~");
//                System.out.println("");
//                System.out.println("Baris-" + input + " datanya : " + datanya);
//                System.out.println("");

                Map DataDistributorDanDepo = cekDistributorDanDepo("PT. ENSEVAL PUTERA MEGATRADING", DataToInsert[0].trim());
                if (DataDistributorDanDepo.get("available").equals("false")) {
                    hapusStokBersesi(sm.getIdSesiImport());
                    JOptionPane.showMessageDialog(null, "DISTRIBUTOR " + DataDistributorDanDepo.get("nama_distributor") + " DAN DEPO " + DataDistributorDanDepo.get("nama_wilayah") + " TIDAK TERDAFTAR DI MASTER, MOHON CEK DEPO DI MASTER WILAYAH", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
                    break;
                } else {
                    new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                    System.out.println("Baris-" + input + " datanya : " + datanya);
                    for (indexBaris = 1; indexBaris < DataToInsert.length; indexBaris++) {
                        if (!DataToInsert[indexBaris].equalsIgnoreCase("0") || !DataToInsert[indexBaris].equalsIgnoreCase("") || !DataToInsert[indexBaris].equalsIgnoreCase("NULL") || !DataToInsert[indexBaris].equalsIgnoreCase("0,0") || !DataToInsert[indexBaris].equalsIgnoreCase("0.0")) {
                            SQL_Insert = "INSERT INTO stockmt (id_sesi_import, id_distributor, distributor, id_depo, "
                                    + " depo, id_barang, kode_barang, nama_barang, qty, bulan, tahun, tgl)"
                                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                            PreparedStatement preSQLInsert = con.prepareStatement(SQL_Insert);
                            preSQLInsert.setString(1, sm.getIdSesiImport());
                            preSQLInsert.setInt(2, Integer.parseInt(DataDistributorDanDepo.get("id_distributor").toString()));
                            preSQLInsert.setString(3, DataDistributorDanDepo.get("nama_distributor").toString());
                            preSQLInsert.setInt(4, Integer.parseInt(DataDistributorDanDepo.get("id_wilayah").toString()));
                            preSQLInsert.setString(5, DataDistributorDanDepo.get("nama_wilayah").toString());
                            Map APIValidasiKodeDanSatuan = kodeBarangTersediaDiKonversiProduk(translateIndexToKodeBarang(indexBaris));
                            preSQLInsert.setInt(6, Integer.parseInt(APIValidasiKodeDanSatuan.get("idmi").toString()));
                            preSQLInsert.setString(7, APIValidasiKodeDanSatuan.get("kodebarangmi").toString());
                            preSQLInsert.setString(8, APIValidasiKodeDanSatuan.get("namabarangmi").toString());
                            preSQLInsert.setFloat(9, Float.parseFloat(DataToInsert[indexBaris].trim().replace(",", ".")));
                            preSQLInsert.setInt(10, frBaru.getBulanClosing());
                            preSQLInsert.setInt(11, frBaru.getTahunClosing());
                            preSQLInsert.setString(12, frBaru.getTanggal());

                            System.out.println("PRE SQL INSERT : " + preSQLInsert);
                            preSQLInsert.executeUpdate();
                        }
                    }
                    indexBaris = 2;
                }
                datanya = "";
                SQL_Insert = "";
            }
            if (frBaru.getSelectedJenisData().equalsIgnoreCase("AKUMULATIF")) {
                System.out.println("AKAN MENGHAPUS DATA AKUMULATIF");
                this.hapusDataAkumulatif(sm.getIdSesiImport(), Integer.toString(frBaru.getBulanClosing()), Integer.toString(frBaru.getTahunClosing()));
            } else if (frBaru.getSelectedJenisData().equalsIgnoreCase("DAILY")) {
                System.out.println("AKAN MENGHAPUS DATA DAILY");
                hapusDataDaily(sm.getIdSesiImport(), Integer.toString(frBaru.getBulanClosing()), Integer.toString(frBaru.getTahunClosing()), frBaru.getTanggal());
            } else {
                System.out.println("GOLPUT");
            }
        } catch (Exception err) {
            hapusStokBersesi(sm.getIdSesiImport());
            err.printStackTrace();
            if (err.getMessage().indexOf("IndexOutOfBoundsException") != -1) {
                JOptionPane.showMessageDialog(null, "Error Pada Converter Distributor, mohon untuk dicek lagi peletakan kolomnya", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "[KOLOM-" + indexBaris + "] " + err.getMessage() + " Pada Data Excel Baris ke-" + input + " Tidak Dapat Ditemukan", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
            }
            JOptionPane.showMessageDialog(null, "Data yg sudah terekam di database telah dihapus karena terdapat error", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
//            con.rollback();
        }
        return null;
    }

    public String hapusCharacter(String text) {
        String textGanti = "";
        try {
            textGanti = text.replaceAll("[^a-zA-Z0-9-_]", " ");
        } catch (Exception e) {
            textGanti = "";
        }
        return textGanti;
    }

    protected void done() {
        JOptionPane.showMessageDialog(null, "Proses Import Telah Selesai", "IMPORT", JOptionPane.INFORMATION_MESSAGE);
    }

    public void mohonTunggu() {
        mohonTungguDialog.showMessageDialog(null, "Proses Eksekusi Sedang Berlangsung", "IMPORT", JOptionPane.INFORMATION_MESSAGE);
    }

    public String cekJanganMiripIndexDepo(int indexPembanding) {
        String result = "";
//        if (DataToInsert[mk.getPakSelected()].trim().equalsIgnoreCase("0") == true) {
//        if (DataToInsert[indexPembanding].trim().equalsIgnoreCase("0") == true) {
        if (indexPembanding == 0) {
            result = "";
//        } else if (DataToInsert[indexPembanding].trim().equalsIgnoreCase(DataToInsert[mk.getDepoSelected()].trim()) == true) {
        } else if (indexPembanding == mk.getDepoSelected()) {
            result = "";
        } else {
//            result = "" + DataToInsert[indexPembanding].trim() + "";
            result = hapusCharacter(DataToInsert[indexPembanding].trim());
        }
        return result;
    }

    public String cekJanganMiripIndexDepoNol(int indexPembanding) {
        String result = "";
//        if (DataToInsert[mk.getPakSelected()].trim().equalsIgnoreCase("0") == true) {
//        if (DataToInsert[indexPembanding].trim().equalsIgnoreCase("0") == true) {
        if (indexPembanding == 0) {
            result = "0";
//        } else if (DataToInsert[indexPembanding].trim().equalsIgnoreCase(DataToInsert[mk.getDepoSelected()].trim()) == true) {
        } else if (indexPembanding == mk.getDepoSelected()) {
            result = "0";
        } else {
            result = "" + DataToInsert[indexPembanding].trim() + "";
//            result = hapusCharacter(DataToInsert[indexPembanding].trim());
        }
        return result;
    }

    public void tambahDepoPerbaruan(String depo) {

        try {
            if (!stackDepoPerbaruan.contains(depo)) {
                if (depo.equalsIgnoreCase("NULL")) {

                } else {
                    stackDepoPerbaruan.add(depo);
                }
            }
            System.out.println("STACK: " + stackDepoPerbaruan);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hapusDataLamaBerdepo() {
        try {
            System.out.println(stackDepoPerbaruan.size());
            for (int stackIndex = 0; stackIndex < stackDepoPerbaruan.size(); stackIndex++) {
                System.out.println(stackDepoPerbaruan.get(stackIndex));

                String sql_DeleteSalesBerdepo = "DELETE FROM stockmt "
                        + "WHERE distributor LIKE ? "
                        + "AND depo LIKE ? "
                        + "AND id_sesi_import != ? "
                        + "AND bulan = ? "
                        + "AND tahun = ? ";
                PreparedStatement prepared_DeleteSalesBerdepo = con.prepareStatement(sql_DeleteSalesBerdepo);
                prepared_DeleteSalesBerdepo.setString(1, mDistributor.getNamaDistribbutor());
                prepared_DeleteSalesBerdepo.setString(2, stackDepoPerbaruan.get(stackIndex));
                prepared_DeleteSalesBerdepo.setString(3, sm.getIdSesiImport());
                prepared_DeleteSalesBerdepo.setInt(4, frBaru.getBulanClosing());
                prepared_DeleteSalesBerdepo.setInt(5, frBaru.getTahunClosing());
                System.out.println("DELETE SQL PREPARED : " + prepared_DeleteSalesBerdepo);
                prepared_DeleteSalesBerdepo.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String customBulan(int bulanClosing) {
        String bulanClosingCus = "";
        try {
            if (bulanClosing < 10) {
                bulanClosingCus = "0" + bulanClosing;
            } else {
                bulanClosingCus = "" + bulanClosing;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bulanClosingCus;
    }

    public Map kodeBarangTersediaDiKonversiProduk(String kodeBarang, String satuanBarang, String QTYPenjualan) {
        Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
        try {
            String strSelect = "SELECT produk.id AS idmi, produk.kode AS kodemi, produk.nama AS namabarangmi, konversisatuan.satuan, konversisatuan.konversi AS konversikekarton \n"
                    + " FROM produk "
                    + " INNER JOIN konversisatuan ON konversisatuan.id_produk = produk.id"
                    + " WHERE produk.kode LIKE ?"
                    + " AND konversisatuan.satuan LIKE ?"
                    + " LIMIT 0,1;";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setString(1, kodeBarang);
            preSQL.setString(2, satuanBarang);
            System.out.println("SQL preSQL : " + preSQL);
            float konversiQtyKarton = 0;
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                System.out.println("QTY PENJUALAN : " + QTYPenjualan + " / " + rs.getInt("konversikekarton"));
                konversiQtyKarton = Float.parseFloat(QTYPenjualan) / rs.getInt("konversikekarton");
                System.out.println("qtyKarton : " + konversiQtyKarton);
                System.out.println("ADA");
                rs.beforeFirst();
                while (rs.next()) {
//                    jsonOfKodeBarangDanSatuan.put("iddistributor", rs.getString("id_distributor"));
                    jsonOfKodeBarangDanSatuan.put("idmi", rs.getString("idmi"));
//                    jsonOfKodeBarangDanSatuan.put("namabarangmi", rs.getString("namabarangmi"));
                    jsonOfKodeBarangDanSatuan.put("kodebarangmi", rs.getString("kodemi"));
                    jsonOfKodeBarangDanSatuan.put("satuan", "KRT");
                    jsonOfKodeBarangDanSatuan.put("konversikekarton", rs.getString("konversikekarton"));
                    jsonOfKodeBarangDanSatuan.put("qtykarton", Float.toString(konversiQtyKarton));
                }

            } else {
                System.out.println("TIDAK ADA");
//                result = kodeBarang;
//                jsonOfKodeBarangDanSatuan.put("iddistributor", Integer.toString(idDistributor));
                jsonOfKodeBarangDanSatuan.put("idmi", "");
//                jsonOfKodeBarangDanSatuan.put("namabarangmi", namaBarang);
                jsonOfKodeBarangDanSatuan.put("kodebarangmi", kodeBarang);
                jsonOfKodeBarangDanSatuan.put("satuan", satuanBarang);
                jsonOfKodeBarangDanSatuan.put("konversikekarton", "");
                jsonOfKodeBarangDanSatuan.put("qtykarton", QTYPenjualan);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonOfKodeBarangDanSatuan;
    }

    public Map kodeBarangTersediaDiKonversiProduk(String kodeBarang) {
        Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
        try {
            String strSelect = "SELECT produk.id AS idmi, produk.kode AS kodemi, produk.nama AS namabarangmi"
                    + " FROM produk "
                    + " WHERE produk.kode LIKE ?"
                    + " LIMIT 0,1;";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setString(1, "%" + kodeBarang + "%");
            System.out.println("SQL preSQL : " + preSQL);
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
//                    jsonOfKodeBarangDanSatuan.put("iddistributor", rs.getString("id_distributor"));
                    jsonOfKodeBarangDanSatuan.put("idmi", rs.getString("idmi"));
                    jsonOfKodeBarangDanSatuan.put("namabarangmi", rs.getString("namabarangmi"));
                    jsonOfKodeBarangDanSatuan.put("kodebarangmi", rs.getString("kodemi"));
//                    jsonOfKodeBarangDanSatuan.put("satuan", "KRT");
//                    jsonOfKodeBarangDanSatuan.put("konversikekarton", rs.getString("konversikekarton"));
//                    jsonOfKodeBarangDanSatuan.put("qtykarton", Float.toString(konversiQtyKarton));
                }
            } else {
                System.out.println("TIDAK ADA");
//                result = kodeBarang;
//                jsonOfKodeBarangDanSatuan.put("iddistributor", Integer.toString(idDistributor));
                jsonOfKodeBarangDanSatuan.put("idmi", "");
                jsonOfKodeBarangDanSatuan.put("namabarangmi", "");
                jsonOfKodeBarangDanSatuan.put("kodebarangmi", kodeBarang);
//                jsonOfKodeBarangDanSatuan.put("satuan", satuanBarang);
//                jsonOfKodeBarangDanSatuan.put("konversikekarton", "");
//                jsonOfKodeBarangDanSatuan.put("qtykarton", QTYPenjualan);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("jsonOfKodeBarangDanSatuan : " + jsonOfKodeBarangDanSatuan);
        return jsonOfKodeBarangDanSatuan;
    }

    public Map cekAvailableDepo(String depoDiExcel) {
        Map<String, String> jsonOfDataDepo = new HashMap<String, String>();
        System.out.println("depodi EXCEL : " + depoDiExcel);
        try {
            System.out.println("DATA di stackAllDepo : " + stackAllDepo);
            System.out.println("DATA di stackAllIDDepo : " + stackAllIDDepo);
            if (stackAllDepo.contains(depoDiExcel)) {
                System.out.println("DEPO ADA");
                jsonOfDataDepo.put("available", "true");
//                System.out.println();
                String idwil = stackAllIDDepo.get(stackAllDepo.indexOf(depoDiExcel));
                System.out.println("IDWIL : " + idwil);
                jsonOfDataDepo.put("id_wilayah", idwil);
                jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
            } else {
                System.out.println("DEPO TIDAK ADA");
                String strSelect = "SELECT * FROM wilayahmt WHERE nama_wilayah LIKE ?";
                PreparedStatement preSQL = con.prepareStatement(strSelect);
                preSQL.setString(1, "%" + depoDiExcel + "%");
                System.out.println("SQL preSQL GET ALL DEPO : " + preSQL);
                ResultSet rs = preSQL.executeQuery();
                if (rs.next()) {
                    rs.beforeFirst();
                    while (rs.next()) {
                        System.out.println("DEPO ADA");
                        jsonOfDataDepo.put("available", "true");
                        String idwil = stackAllIDDepo.get(stackAllDepo.indexOf(depoDiExcel));
                        System.out.println("IDWIL : " + idwil);
                        jsonOfDataDepo.put("id_wilayah", idwil);
                        jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
                    }
                } else {
                    jsonOfDataDepo.put("available", "false");
                    jsonOfDataDepo.put("id_wilayah", "");
                    jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("DATA DEPO JSON: " + jsonOfDataDepo);
        return jsonOfDataDepo;
    }

    public String translateIndexToKodeBarang(int indexBarisExcel) {
        System.out.println("indexBarisExcel : " + indexBarisExcel);
        String kode_barang = "";
        String[] kodeBarangTranslators = {
//            "",
            "",
            "LDK07",
            "LDK08",
            "LDK38",
            "LDK42",
            "DSK06",
            "DSK08",
            "DSK09",
            "DKU03",
            "DBL03",
            "DKR03",
            "DSG03",
            "DGL02",
            "DLD03",
            "DSO02"};
        kode_barang = kodeBarangTranslators[indexBarisExcel];
        System.out.println("HASIL TRANSLATOR : " + kode_barang);
        return kode_barang;
    }

    public void getAllDepo(int idDistributor) {
        try {
            String strSelect = "SELECT distributormt.id_distributor, distributormt.nama_distributor, wilayahmt.id_wilayah, wilayahmt.nama_wilayah FROM distributormt "
                    + "RIGHT JOIN wilayahmt ON wilayahmt.id_distributor = distributormt.id_distributor "
                    + "WHERE distributormt.id_distributor = ?";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setInt(1, idDistributor);
            System.out.println("SQL preSQL GET ALL DEPO : " + preSQL);
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    String depoCustom = rs.getString("nama_wilayah");
                    String idDepo = rs.getString("id_wilayah");
                    addToStackAllDepo(depoCustom, idDepo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addToStackAllDepo(String depo, String id) {
        try {
            if (!stackAllDepo.contains(depo)) {
                if (depo.equalsIgnoreCase("NULL")) {

                } else {
                    stackAllDepo.add(depo);
                    stackAllIDDepo.add(id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map cekDistributorDanDepo(String distributorInExcel, String depoInExcel) {
        Map<String, String> jsonOfDataDepo = new HashMap<String, String>();
        System.out.println("Distributor : " + distributorInExcel + " / " + "Depo : " + depoInExcel);
        try {
            String sqlCek = "SELECT distributormt.* , wilayahmt.id_wilayah, wilayahmt.nama_wilayah FROM distributormt INNER JOIN wilayahmt ON wilayahmt.id_distributor = distributormt.id_distributor "
                    + " WHERE distributormt.nama_distributor LIKE ?"
                    + " AND wilayahmt.nama_wilayah LIKE ?";
            PreparedStatement preSQLCek = con.prepareStatement(sqlCek);
            preSQLCek.setString(1, "%" + distributorInExcel + "%");
            preSQLCek.setString(2, "%" + depoInExcel + "%");
            System.out.println("PRESLCEK : " + preSQLCek);
            ResultSet rs = preSQLCek.executeQuery();
            if (rs.first()) {
//                rs.beforeFirst();
                System.out.println("DEPO DAN DISTRIBUTOR ADA " + rs.getString("nama_distributor") + "/" + rs.getString("nama_wilayah"));
                jsonOfDataDepo.put("available", "true");
                jsonOfDataDepo.put("id_distributor", rs.getString("id_distributor"));
                jsonOfDataDepo.put("nama_distributor", rs.getString("nama_distributor"));
                jsonOfDataDepo.put("id_wilayah", rs.getString("id_wilayah"));
                jsonOfDataDepo.put("nama_wilayah", rs.getString("nama_wilayah"));
            } else {
                System.out.println("DEPO DAN DISTRIBUTOR TIDAK ADA");
                jsonOfDataDepo.put("available", "false");
                jsonOfDataDepo.put("id_distributor", "");
                jsonOfDataDepo.put("nama_distributor", distributorInExcel);
                jsonOfDataDepo.put("id_wilayah", "");
                jsonOfDataDepo.put("nama_wilayah", depoInExcel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonOfDataDepo;
    }

    public void hapusStokBersesi(String id_sesi) {
        try {
            String sqlDelete = "DELETE FROM stockmt WHERE id_sesi_import = ?";
            PreparedStatement preSqlDelete = con.prepareStatement(sqlDelete);
            preSqlDelete.setString(1, id_sesi);
            System.out.println("PRESQL : " + preSqlDelete);
            preSqlDelete.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hapusDataDaily(String id_sesi, String closing, String tahun_closing, String tgl) {
        try {
            String sqlDelete = "DELETE FROM stockmt"
                    + " WHERE id_sesi_import != ?"
                    + " AND bulan = ?"
                    + " AND tahun = ?"
                    + " AND tgl = ?";
            PreparedStatement preSqlDelete = con.prepareStatement(sqlDelete);
            preSqlDelete.setString(1, id_sesi);
            preSqlDelete.setString(2, closing);
            preSqlDelete.setString(3, tahun_closing);
            preSqlDelete.setString(4, tgl);
            System.out.println("PRESQL : " + preSqlDelete);
            preSqlDelete.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hapusDataAkumulatif(String id_sesi, String closing, String tahun_closing) {
        try {
            String sqlDelete = "DELETE FROM stock"
                    + " WHERE id_sesi_import != ?"
                    + " AND bulan = ?"
                    + " AND tahun = ?";
            PreparedStatement preSqlDelete = con.prepareStatement(sqlDelete);
            preSqlDelete.setString(1, id_sesi);
            preSqlDelete.setString(2, closing);
            preSqlDelete.setString(3, tahun_closing);
            System.out.println("PRESQL : " + preSqlDelete);
            preSqlDelete.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
