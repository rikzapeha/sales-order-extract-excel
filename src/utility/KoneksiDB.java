package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class KoneksiDB {
	
	private static Connection con;
	private static String url,username,password;
	
	public static Connection getKoneksi(){
		if(con==null){
			url = "jdbc:mysql://192.168.1.207/insentifdist";
			username = "insentif";
			password = "insentif";
			
			try {
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				con = DriverManager.getConnection(url, username, password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return con;
	}

}

